; The version of Drupal the profile is built for. You must include both a
; major and a minor release - just specifying 6.x won't work in drupal.org-make (but not sure if this works)

core = 7.x
api = 2

; The OpenProducer Foundation

projects[openproducer_core][type] = "module"
projects[openproducer_core][download][type] = "git"
projects[openproducer_core][download][url] = "git://github.com/openlearninglabs/openproducer_core.git"
projects[openproducer_core][download][branch] = "7.x-1.x"
projects[openproducer_core][subdir] = openproducer

projects[openproducer_designer][type] = "module"
projects[openproducer_designer][download][type] = "git"
projects[openproducer_designer][download][url] = "git://github.com/openlearninglabs/openproducer_designer.git"
projects[openproducer_designer][download][download][branch] = "7.x-1.x"
projects[openproducer_designer][subdir] = openproducer

projects[openproducer_events][type] = "module"
projects[openproducer_events][download][type] = "git"
projects[openproducer_events][download][url] = "git://github.com/openlearninglabs/openproducer_events.git"
projects[openproducer_events][download][download][branch] = "7.x-1.x"
projects[openproducer_events][subdir] = openproducer

projects[openproducer_music][type] = "module"
projects[openproducer_music][download][type] = "git"
projects[openproducer_music][download][url] = "git://github.com/openlearninglabs/openproducer_music.git"
projects[openproducer_music][download][download][branch] = "7.x-1.x""
projects[openproducer_music][subdir] = openproducer

projects[openproducer_news][type] = "module"
projects[openproducer_news][download][type] = "git"
projects[openproducer_news][download][url] = "git://github.com/openlearninglabs/openproducer_news.git"
projects[openproducer_news][download][download][branch] = "7.x-1.x""
projects[openproducer_news][subdir] = openproducer

projects[openproducer_pages][type] = "module"
projects[openproducer_pages][download][type] = "git"
projects[openproducer_pages][download][url] = "git://github.com/openlearninglabs/openproducer_pages.git"
projects[openproducer_pages][download][download][branch] = "7.x-1.x"
projects[openproducer_pages][subdir] = openproducer

projects[openproducer_photos][type] = "module"
projects[openproducer_photos][download][type] = "git"
projects[openproducer_photos][download][url] = "git://github.com/openlearninglabs/openproducer_photos.git"
projects[openproducer_photos][download][download][branch] = "7.x-1.x"
projects[openproducer_photos][subdir] = openproducer

projects[openproducer_portfolio][type] = "module"
projects[openproducer_portfolio][download][type] = "git"
projects[openproducer_portfolio][download][url] = "git://github.com/openlearninglabs/openproducer_portfolio.git"
projects[openproducer_portfolio][download][download][branch] = "7.x-1.x"
projects[openproducer_portfolio][subdir] = openproducer

projects[openproducer_productions][type] = "module"
projects[openproducer_productions][download][type] = "git"
projects[openproducer_productions][download][url] = "git://github.com/openlearninglabs/openproducer_productions.git"
projects[openproducer_productions][download][download][branch] = "7.x-1.x"
projects[openproducer_productions][subdir] = openproducer

projects[openproducer_social][type] = "module"
projects[openproducer_social][download][type] = "git"
projects[openproducer_social][download][url] = "git://github.com/openlearninglabs/openproducer_social.git"
projects[openproducer_social][download][download][branch] = "7.x-1.x"
projects[openproducer_social][subdir] = openproducer

projects[openproducer_video][type] = "module"
projects[openproducer_video][download][type] = "git"
projects[openproducer_video][download][url] = "git://github.com/openlearninglabs/openproducer_video.git"
projects[openproducer_video][download][branch] = "7.x-1.x"
projects[openproducer_video][subdir] = openproducer

projects[openproducer_widgets][type] = "module"
projects[openproducer_widgets][download][type] = "git"
projects[openproducer_widgets][download][url] = "git://github.com/openlearninglabs/openproducer_widgets.git"
projects[openproducer_widgets][download][branch] = "7.x-1.x"
projects[openproducer_widgets][subdir] = openproducer


; Panopoly Foundation

projects[panopoly_core][version] = 1.29
projects[panopoly_core][subdir] = panopoly

projects[panopoly_images][version] = 1.29
projects[panopoly_images][subdir] = panopoly

projects[panopoly_theme][version] = 1.29
projects[panopoly_theme][subdir] = panopoly

projects[panopoly_magic][version] = 1.29
projects[panopoly_magic][subdir] = panopoly

projects[panopoly_widgets][version] = 1.29
projects[panopoly_widgets][subdir] = panopoly
projects[panopoly_widgets][patch][2473495] = https://www.drupal.org/files/issues/clicking_images_in_my-2473495-7.patch
projects[panopoly_widgets][patch][2477397] = https://www.drupal.org/files/issues/2477397-panopoly_widgets-file_entity-2.patch


projects[panopoly_admin][version] = 1.29
projects[panopoly_admin][subdir] = panopoly

projects[panopoly_users][version] = 1.29
projects[panopoly_users][subdir] = panopoly

; The Panopoly Toolset

projects[panopoly_pages][version] = 1.29
projects[panopoly_pages][subdir] = panopoly

projects[panopoly_wysiwyg][version] = 1.29
projects[panopoly_wysiwyg][subdir] = panopoly


; Themes

projects[pushtape_bootstrap][type] = "theme"
projects[pushtape_bootstrap][download][type] = "git"
projects[pushtape_bootstrap][download][url] = "git://github.com/gusaus/pushtape_bootstrap.git"
projects[pushtape_bootstrap][download][branch] = "7.x-1.x-panels"

projects[openproducer_radix][type] = "theme"
projects[openproducer_radix][download][type] = "git"
projects[openproducer_radix][download][url] = "git://github.com/openlearninglabs/openproducer_radix.git"
projects[openproducer_radix][download][branch] = "7.x-1.x"

; Base theme.
projects[radix][version] = 3.0-rc4
projects[shiny][version] = 1.7

; Libraries.

libraries[colorbox][download][type] = get
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/tarball/master
libraries[colorbox][destination] = libraries
libraries[flexslider][download][type] = get
libraries[flexslider][download][url] = https://github.com/woothemes/FlexSlider/tarball/master
libraries[flexslider][destination] = libraries
libraries[soundmanager2][download][type] = get
libraries[soundmanager2][download][url] = https://github.com/scottschiller/SoundManager2/tarball/master
libraries[soundmanager2][destination] = libraries
libraries[plupload][download][type] = "get"
libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v1.5.8.zip"
libraries[plupload][patch][1903850] = "http://drupal.org/files/issues/plupload-1_5_8-rm_examples-1903850-16.patch"
libraries[genericons][download][type] = get
libraries[genericons][download][url] = https://github.com/Automattic/Genericons/tarball/master
libraries[genericons][destination] = libraries
