<?php
// Plugin definition
$plugin = array(
  'title' => t('Bowie'),
  'icon' => 'bowie.png',
  'category' => t('OpenProducer'),
  'theme' => 'bowie',
  'regions' => array(
    'contentmain' => t('Content'),
  ),
);
