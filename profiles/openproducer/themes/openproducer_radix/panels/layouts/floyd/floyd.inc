<?php
// Plugin definition
$plugin = array(
  'title' => t('Floyd'),
  'icon' => 'floyd.png',
  'category' => t('OpenProducer'),
  'theme' => 'floyd',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
