<?php
// Plugin definition
$plugin = array(
  'title' => t('Coltrane'),
  'icon' => 'coltrane.png',
  'category' => t('OpenProducer'),
  'theme' => 'coltrane',
  'regions' => array(
    'header' => t('Header'),
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
    'footer' => t('Footer'),
  ),
);
