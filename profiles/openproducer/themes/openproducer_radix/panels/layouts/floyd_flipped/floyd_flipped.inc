<?php
// Plugin definition
$plugin = array(
  'title' => t('Floyd Flipped'),
  'icon' => 'floyd-flipped.png',
  'category' => t('OpenProducer'),
  'theme' => 'floyd_flipped',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
