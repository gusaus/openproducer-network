<?php
// Plugin definition
$plugin = array(
  'title' => t('Bonham'),
  'icon' => 'bonham.png',
  'category' => t('OpenProducer'),
  'theme' => 'bonham',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
