<?php
// Plugin definition
$plugin = array(
  'title' => t('Bonham Flipped'),
  'icon' => 'bonham-flipped.png',
  'category' => t('OpenProducer'),
  'theme' => 'bonham_flipped',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
