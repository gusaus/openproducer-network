api = 2
core = 7.x

;Include the definition for how to build Drupal core directly, including patches:
includes[] = https://raw.github.com/openlearninglabs/openproducer/7.x-1.x/drupal-org-core.make

projects[openproducer][type] = profile
projects[openproducer][download][type] = git
projects[openproducer][download][url] = git://github.com/openlearninglabs/openproducer.git
projects[openproducer][download][branch] = 7.x-1.x
