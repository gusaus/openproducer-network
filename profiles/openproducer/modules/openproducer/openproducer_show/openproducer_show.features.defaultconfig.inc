<?php
/**
 * @file
 * openproducer_show.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function openproducer_show_defaultconfig_features() {
  return array(
    'openproducer_show' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function openproducer_show_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create episode content'.
  $permissions['create episode content'] = array(
    'name' => 'create episode content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create show content'.
  $permissions['create show content'] = array(
    'name' => 'create show content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any episode content'.
  $permissions['delete any episode content'] = array(
    'name' => 'delete any episode content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any show content'.
  $permissions['delete any show content'] = array(
    'name' => 'delete any show content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own episode content'.
  $permissions['delete own episode content'] = array(
    'name' => 'delete own episode content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own show content'.
  $permissions['delete own show content'] = array(
    'name' => 'delete own show content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any episode content'.
  $permissions['edit any episode content'] = array(
    'name' => 'edit any episode content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any show content'.
  $permissions['edit any show content'] = array(
    'name' => 'edit any show content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own episode content'.
  $permissions['edit own episode content'] = array(
    'name' => 'edit own episode content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own show content'.
  $permissions['edit own show content'] = array(
    'name' => 'edit own show content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
