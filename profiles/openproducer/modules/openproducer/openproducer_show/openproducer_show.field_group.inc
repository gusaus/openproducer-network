<?php
/**
 * @file
 * openproducer_show.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function openproducer_show_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_artist_info|node|video|form';
  $field_group->group_name = 'group_artist_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'episode';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_fields';
  $field_group->data = array(
    'label' => 'Artist info',
    'weight' => '15',
    'children' => array(
      0 => 'field_artist',
      1 => 'field_genre',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-artist-info field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_artist_info|node|video|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_media|node|episode|form';
  $field_group->group_name = 'group_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'episode';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_fields';
  $field_group->data = array(
    'label' => 'Media',
    'weight' => '10',
    'children' => array(
      0 => 'field_video_file',
      1 => 'field_photos',
      2 => 'field_audio',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-media field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_media|node|episode|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_show_info|node|video|form';
  $field_group->group_name = 'group_show_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'episode';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_fields';
  $field_group->data = array(
    'label' => 'Show info',
    'weight' => '9',
    'children' => array(
      0 => 'field_season_episode',
      1 => 'field_show_date',
      2 => 'field_description',
      3 => 'field_video_type',
      4 => 'field_video_category',
      5 => 'field_headline',
      6 => 'field_program',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-show-info field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_show_info|node|video|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Artist info');
  t('Media');
  t('Show info');

  return $field_groups;
}
