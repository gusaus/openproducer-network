<?php
/**
 * @file
 * openproducer_show.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function openproducer_show_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer panelizer node episode content'.
  $permissions['administer panelizer node episode content'] = array(
    'name' => 'administer panelizer node episode content',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node episode layout'.
  $permissions['administer panelizer node episode layout'] = array(
    'name' => 'administer panelizer node episode layout',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node show content'.
  $permissions['administer panelizer node show content'] = array(
    'name' => 'administer panelizer node show content',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: 'administer panelizer node show layout'.
  $permissions['administer panelizer node show layout'] = array(
    'name' => 'administer panelizer node show layout',
    'roles' => array(),
    'module' => 'panelizer',
  );

  return $permissions;
}
