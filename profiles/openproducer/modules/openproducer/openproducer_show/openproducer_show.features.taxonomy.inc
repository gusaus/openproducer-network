<?php
/**
 * @file
 * openproducer_show.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function openproducer_show_taxonomy_default_vocabularies() {
  return array(
    'artists' => array(
      'name' => 'Artists',
      'machine_name' => 'artists',
      'description' => 'Associate episodes and shows with artists.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'episode_categories' => array(
      'name' => 'Episode categories',
      'machine_name' => 'episode_categories',
      'description' => 'Categories specifically for episodes',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'episode_type' => array(
      'name' => 'Episode type',
      'machine_name' => 'episode_type',
      'description' => '',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'genre' => array(
      'name' => 'Genre',
      'machine_name' => 'genre',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -8,
    ),
    'program_type' => array(
      'name' => 'Program type',
      'machine_name' => 'program_type',
      'description' => 'Type of program, show, or series',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
