<?php
/**
 * @file
 * openproducer_events.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function openproducer_events_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'event';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:event:default';
  $panelizer->css_id = 'events';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'radix_moscone_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'header' => NULL,
      'half' => NULL,
      'quarter1' => NULL,
      'quarter2' => NULL,
      'footer' => NULL,
      'contentfooter' => NULL,
      'contentheader' => NULL,
      'contentcolumn1' => NULL,
      'contentcolumn2' => NULL,
    ),
    'contentheader' => array(
      'style' => '0',
    ),
    'contentcolumn1' => array(
      'style' => '0',
    ),
    'contentcolumn2' => array(
      'style' => '0',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'a0e11f53-5634-442e-ae6d-7a020c7826c5';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:event:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ed7a5620-f9dd-4293-80d0-41ae581359ee';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ed7a5620-f9dd-4293-80d0-41ae581359ee';
  $display->content['new-ed7a5620-f9dd-4293-80d0-41ae581359ee'] = $pane;
  $display->panels['contentmain'][0] = 'new-ed7a5620-f9dd-4293-80d0-41ae581359ee';
  $pane = new stdClass();
  $pane->pid = 'new-75ac8e5c-0287-4dd0-8890-386beb91d451';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_episode';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'entityreference_entity_view',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(
      'view_mode' => 'teaser',
      'links' => 1,
      'use_content_language' => 1,
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '75ac8e5c-0287-4dd0-8890-386beb91d451';
  $display->content['new-75ac8e5c-0287-4dd0-8890-386beb91d451'] = $pane;
  $display->panels['contentmain'][1] = 'new-75ac8e5c-0287-4dd0-8890-386beb91d451';
  $pane = new stdClass();
  $pane->pid = 'new-d40dc776-faee-48d7-ac55-471996a2042a';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'shows-panel_pane_13';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'd40dc776-faee-48d7-ac55-471996a2042a';
  $display->content['new-d40dc776-faee-48d7-ac55-471996a2042a'] = $pane;
  $display->panels['contentmain'][2] = 'new-d40dc776-faee-48d7-ac55-471996a2042a';
  $pane = new stdClass();
  $pane->pid = 'new-26d795c5-41eb-4d65-b566-da8017bac39b';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'events-panel_pane_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'more_link' => 0,
    'use_pager' => 0,
    'pager_id' => '0',
    'items_per_page' => '0',
    'offset' => '0',
    'fields_override' => array(
      'field_featured_image' => 1,
      'field_program' => 0,
      'title' => 1,
      'field_event_venue' => 1,
      'field_event_date' => 1,
      'field_event_date_1' => 1,
      'field_tickets' => 1,
    ),
    'exposed' => array(
      'sort_order' => 'ASC',
      'sort_by' => 'field_event_date_value_1',
    ),
    'override_title' => '',
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'view_settings' => 'fields',
    'header_type' => 'titles',
    'view_mode' => 'teaser',
    'widget_title' => 'Upcoming Concerts',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '26d795c5-41eb-4d65-b566-da8017bac39b';
  $display->content['new-26d795c5-41eb-4d65-b566-da8017bac39b'] = $pane;
  $display->panels['contentmain'][3] = 'new-26d795c5-41eb-4d65-b566-da8017bac39b';
  $pane = new stdClass();
  $pane->pid = 'new-fe1c0904-9a8f-498d-a39b-0d631d972731';
  $pane->panel = 'header';
  $pane->type = 'views_panes';
  $pane->subtype = 'events-panel_pane_5';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'hide_classes' => array(
        'hidden-xs' => 0,
        'hidden-sm' => 0,
        'hidden-md' => 0,
        'hidden-lg' => 0,
      ),
      'additional_classes' => array(
        'clearfix' => 'clearfix',
        'pull-left' => 0,
        'pull-right' => 0,
      ),
    ),
    'style' => 'bootstrap_styles:jumbotron',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fe1c0904-9a8f-498d-a39b-0d631d972731';
  $display->content['new-fe1c0904-9a8f-498d-a39b-0d631d972731'] = $pane;
  $display->panels['header'][0] = 'new-fe1c0904-9a8f-498d-a39b-0d631d972731';
  $pane = new stdClass();
  $pane->pid = 'new-7ffc7eed-9c48-4ea0-904d-b00f68937b6e';
  $pane->panel = 'sidebar';
  $pane->type = 'views_panes';
  $pane->subtype = 'events-panel_pane_4';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
  );
  $pane->cache = array();
  $pane->style = array(
    'style' => 'bootstrap_styles:well',
    'settings' => array(
      'hide_classes' => array(
        'hidden-xs' => 0,
        'hidden-sm' => 0,
        'hidden-md' => 0,
        'hidden-lg' => 0,
      ),
      'additional_classes' => array(
        'clearfix' => 'clearfix',
        'pull-left' => 0,
        'pull-right' => 0,
      ),
    ),
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7ffc7eed-9c48-4ea0-904d-b00f68937b6e';
  $display->content['new-7ffc7eed-9c48-4ea0-904d-b00f68937b6e'] = $pane;
  $display->panels['sidebar'][0] = 'new-7ffc7eed-9c48-4ea0-904d-b00f68937b6e';
  $pane = new stdClass();
  $pane->pid = 'new-2162660e-224e-4978-a58b-84b75b0b56cd';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_performers';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'title',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'hide_classes' => array(
        'hidden-xs' => 0,
        'hidden-sm' => 0,
        'hidden-md' => 0,
        'hidden-lg' => 0,
      ),
      'additional_classes' => array(
        'clearfix' => 'clearfix',
        'pull-left' => 0,
        'pull-right' => 0,
      ),
    ),
    'style' => 'bootstrap_styles:well',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '2162660e-224e-4978-a58b-84b75b0b56cd';
  $display->content['new-2162660e-224e-4978-a58b-84b75b0b56cd'] = $pane;
  $display->panels['sidebar'][1] = 'new-2162660e-224e-4978-a58b-84b75b0b56cd';
  $pane = new stdClass();
  $pane->pid = 'new-cc571ec6-9024-481e-ab17-68a388cf8bc4';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_artwork';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'panopoly_image_full',
      'image_link' => '',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'hide_classes' => array(
        'hidden-xs' => 0,
        'hidden-sm' => 0,
        'hidden-md' => 0,
        'hidden-lg' => 0,
      ),
      'additional_classes' => array(
        'clearfix' => 'clearfix',
        'pull-left' => 0,
        'pull-right' => 0,
      ),
    ),
    'style' => 'bootstrap_styles:well',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'cc571ec6-9024-481e-ab17-68a388cf8bc4';
  $display->content['new-cc571ec6-9024-481e-ab17-68a388cf8bc4'] = $pane;
  $display->panels['sidebar'][2] = 'new-cc571ec6-9024-481e-ab17-68a388cf8bc4';
  $pane = new stdClass();
  $pane->pid = 'new-7d34edf9-db4e-4174-954a-b0c92a1d3558';
  $pane->panel = 'sidebar';
  $pane->type = 'views_panes';
  $pane->subtype = 'events-panel_pane_3';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'more_link' => 1,
    'items_per_page' => '3',
    'fields_override' => array(
      'field_featured_image' => 1,
      'title' => 0,
      'field_event_date' => 0,
      'field_event_venue' => 0,
      'field_event_city' => 0,
      'field_event_state' => 0,
      'nothing' => 1,
    ),
    'override_title' => '',
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'view_settings' => 'fields',
    'header_type' => 'none',
    'view_mode' => 'teaser',
    'widget_title' => 'Upcoming Shows',
  );
  $pane->cache = array();
  $pane->style = array(
    'style' => 'default',
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '7d34edf9-db4e-4174-954a-b0c92a1d3558';
  $display->content['new-7d34edf9-db4e-4174-954a-b0c92a1d3558'] = $pane;
  $display->panels['sidebar'][3] = 'new-7d34edf9-db4e-4174-954a-b0c92a1d3558';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:event:default'] = $panelizer;

  return $export;
}
