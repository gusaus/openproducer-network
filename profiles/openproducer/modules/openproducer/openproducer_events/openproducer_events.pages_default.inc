<?php
/**
 * @file
 * openproducer_events.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function openproducer_events_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'events';
  $page->task = 'page';
  $page->admin_title = 'Events';
  $page->admin_description = '';
  $page->path = 'events';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Events',
    'weight' => 0,
    'name' => 'main-menu',
    'parent' => 'main-menu:0',
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_events_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'events';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Landing page',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'events',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_moscone_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
    'contentmain' => array(
      'style' => '0',
    ),
    'sidebar' => array(
      'style' => '0',
    ),
  );
  $display->cache = array();
  $display->title = 'Shows';
  $display->uuid = 'b17472e1-c860-4aec-a51c-31316709c674';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_events_panel_context';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b74d26b8-c6c1-4a1c-ac34-f619776d8756';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'events-panel_pane_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'more_link' => 0,
    'use_pager' => 0,
    'pager_id' => '0',
    'items_per_page' => '0',
    'fields_override' => array(
      'field_featured_image' => 1,
      'field_program' => 1,
      'title' => 1,
      'field_event_venue' => 1,
      'field_event_date' => 1,
      'field_event_date_1' => 1,
      'field_tickets' => 1,
    ),
    'exposed' => array(
      'sort_order' => 'ASC',
      'sort_by' => 'field_event_date_value_1',
    ),
    'override_title' => '',
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'view_settings' => 'fields',
    'header_type' => 'titles',
    'view_mode' => 'teaser',
    'widget_title' => '',
    'offset' => '1',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b74d26b8-c6c1-4a1c-ac34-f619776d8756';
  $display->content['new-b74d26b8-c6c1-4a1c-ac34-f619776d8756'] = $pane;
  $display->panels['contentmain'][0] = 'new-b74d26b8-c6c1-4a1c-ac34-f619776d8756';
  $pane = new stdClass();
  $pane->pid = 'new-2225d209-d25d-4eec-88f6-1a0a4964d534';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'events-panel_pane_11';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'more_link' => 0,
    'use_pager' => 0,
    'pager_id' => '0',
    'items_per_page' => '0',
    'offset' => '0',
    'fields_override' => array(
      'field_featured_image' => 1,
      'field_program' => 1,
      'title' => 1,
      'field_event_venue' => 1,
      'field_event_date' => 1,
      'field_event_date_1' => 0,
      'field_tickets' => 0,
    ),
    'exposed' => array(
      'sort_order' => 'DESC',
      'sort_by' => 'field_event_date_value_1',
    ),
    'override_title' => '',
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'view_settings' => 'fields',
    'header_type' => 'titles',
    'view_mode' => 'teaser',
    'widget_title' => 'Previous Concerts',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '2225d209-d25d-4eec-88f6-1a0a4964d534';
  $display->content['new-2225d209-d25d-4eec-88f6-1a0a4964d534'] = $pane;
  $display->panels['contentmain'][1] = 'new-2225d209-d25d-4eec-88f6-1a0a4964d534';
  $pane = new stdClass();
  $pane->pid = 'new-5bf82e3b-0276-4e92-a72c-9d09bae63717';
  $pane->panel = 'header';
  $pane->type = 'views_panes';
  $pane->subtype = 'events-panel_pane_9';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'hide_classes' => array(
        'hidden-xs' => 0,
        'hidden-sm' => 0,
        'hidden-md' => 0,
        'hidden-lg' => 0,
      ),
      'additional_classes' => array(
        'clearfix' => 'clearfix',
        'pull-left' => 0,
        'pull-right' => 0,
      ),
    ),
    'style' => 'bootstrap_styles:jumbotron',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5bf82e3b-0276-4e92-a72c-9d09bae63717';
  $display->content['new-5bf82e3b-0276-4e92-a72c-9d09bae63717'] = $pane;
  $display->panels['header'][0] = 'new-5bf82e3b-0276-4e92-a72c-9d09bae63717';
  $pane = new stdClass();
  $pane->pid = 'new-d8aa7503-d9e2-4671-86ef-40ec40d7f647';
  $pane->panel = 'sidebar';
  $pane->type = 'views_panes';
  $pane->subtype = 'shows-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'use_pager' => 0,
    'pager_id' => '0',
    'items_per_page' => '5',
    'offset' => '0',
    'exposed' => array(
      'field_video_type_tid' => array(
        104 => '104',
      ),
      'field_featured_status_value' => 'All',
      'field_video_category_tid' => array(
        125 => '125',
      ),
      'sort_order' => 'DESC',
      'sort_by' => 'field_show_date_value',
    ),
    'override_title' => '',
    'override_title_text' => '',
    'override_title_heading' => 'h2',
    'view_settings' => 'fields',
    'header_type' => 'titles',
    'view_mode' => 'teaser',
    'widget_title' => 'Performance Videos',
    'fields_override' => array(
      'path' => 0,
      'field_video_file' => 1,
      'title' => 1,
      'field_video_type' => 0,
      'field_video_category' => 0,
      'field_season_episode' => 0,
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'hide_classes' => array(
        'hidden-xs' => 0,
        'hidden-sm' => 0,
        'hidden-md' => 0,
        'hidden-lg' => 0,
      ),
      'additional_classes' => array(
        'clearfix' => 'clearfix',
        'pull-left' => 0,
        'pull-right' => 0,
      ),
    ),
    'style' => 'bootstrap_styles:well',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd8aa7503-d9e2-4671-86ef-40ec40d7f647';
  $display->content['new-d8aa7503-d9e2-4671-86ef-40ec40d7f647'] = $pane;
  $display->panels['sidebar'][0] = 'new-d8aa7503-d9e2-4671-86ef-40ec40d7f647';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['events'] = $page;

  return $pages;

}
