; Drupal.org release file.
core = 7.x
api = 2

;Contrib

projects[addthis][version] = 4.0-alpha6
projects[addthis][subdir] = contrib

projects[better_formats][version] = 1.0-beta2
projects[better_formats][subdir] = contrib

projects[calendar][version] = 3.5
projects[calendar][subdir] = contrib

projects[colorbox][version] = 2.13
projects[colorbox][subdir] = contrib

projects[colorizer][version] = 1.10
projects[colorizer][subdir] = contrib

projects[context][version] = 3.7
projects[context][subdir] = contrib

projects[dynamic_background][version] = 2.x-dev
projects[dynamic_background][subdir] = contrib

projects[entity_view_mode][version] = 1.0-rc1
projects[entity_view_mode][subdir] = contrib

projects[field_permissions][version] = 1.0
projects[field_permissions][subdir] = contrib

projects[file_entity_link][version] = 1.0-alpha3
projects[file_entity_link][subdir] = contrib

projects[flexslider][version] = 2.0-rc2
projects[flexslider][subdir] = contrib

projects[google_analytics][version] = 2.4
projects[google_analytics][subdir] = contrib

projects[media_colorbox][version] = 1.x-dev
projects[media_colorbox][subdir] = contrib

projects[metatag][version] = 1.22
projects[metatag][subdir] = contrib

projects[multiform][version] = 1.4
projects[multiform][subdir] = contrib

projects[multiupload_filefield_widget][version] = 1.13
projects[multiupload_filefield_widget][subdir] = contrib

projects[panels_bootstrap_styles][version] = 1.x-dev
projects[panels_bootstrap_styles][subdir] = contrib

projects[plupload][version] = 1.x-dev
projects[plupload][subdir] = contrib

projects[radix_colorizer][version] = 1.x-dev
projects[radix_colorizer][subdir] = contrib

projects[role_export][version] = 1.x-dev
projects[role_export][subdir] = contrib

projects[rules][version] = 2.10
projects[rules][subdir] = contrib

projects[rules_linkevent][version] = 1.0-beta1
projects[rules_linkevent][subdir] = contrib


projects[views_bootstrap][version] = 3.2
projects[views_bootstrap][subdir] = contrib







